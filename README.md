# Revenants-2D-Demo

2D test of a procedural maze generator game made in GoDot.

Uses the algorithms found here: https://github.com/juanbecerra0/javaMazeGenerator

Used to test maze generation for my game found here: https://github.com/juanbecerra0/Revenants-Game
