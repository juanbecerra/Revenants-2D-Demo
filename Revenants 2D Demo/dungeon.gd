extends Node2D

# Variables for imported
onready var troll_resource = preload("res://troll.tscn")
onready var wall_resource = preload("res://MazeGen/wall.tscn")
onready var base_resource = preload("res://MazeGen/base.tscn")
onready var door_resource = preload("res://MazeGen/door1.tscn")

var troll

# Called when the node enters the scene tree for the first time.
func _ready():
	
	# Variables for dungeon generation
	var x = 15
	var y = 15
	var treasureChance = 0.1
	var enemyChance = 0.3
	var seedValue = -1
	
	generate_level(x, y, treasureChance, enemyChance, seedValue)

# Level generation algorithm
func generate_level(x, y, treasureChance, enemyChance, seedValue):
	# Create new maze object
	var arrObj = preload("res://MazeGen/MazeGen.cs")
	var node = arrObj.new(x, y, treasureChance, enemyChance, seedValue)
	node.generateMaze()
	
	# Store maze array as variable
	var non_form_array = node.getArray()
	var iterator = 0
	var vec = Vector2(0,0)
	
	# For enterence generation
	var player_spot_found = false
	var player_vec = Vector2(0,0)
	
	# For exit generation
	var door_spot_found = false
	var door_vec = Vector2(0,0)
	
	# Generate instances into dungeon
	for i in range(x*2):
		for j in range(y*2):
			vec = Vector2((((j + 1)*128)-(j*64)-(i*64)), (((i+1)*64)-(i*32)+(j*32)))
			if non_form_array[iterator] == 0:
				var wall = wall_resource.instance()
				wall.position = vec
				self.add_child(wall)
			else:
				var base = base_resource.instance()
				base.position = vec
				self.add_child(base)
					
				if non_form_array[iterator] == 2 && !player_spot_found:
					# This is the point where the player will spawn
					player_vec = vec
					player_spot_found = true
				else:
					if non_form_array[iterator] == 3 && !door_spot_found:
						# This is the point where the exit will spawn
						door_vec = vec
						door_spot_found = true
			
			iterator = iterator + 1
	
	# Generate player starting point
	troll = troll_resource.instance()
	troll.position = player_vec
	self.add_child(troll)
	
	# Generate player exit point
	var door = door_resource.instance()
	door.position = door_vec
	door.connect("level_finished", self, "_on_door1_level_finished")
	self.add_child(door)

func _on_door1_level_finished():
	troll.playing = false
	get_tree().change_scene('res://MainMenu/MiscScenes/EndScreen.tscn')


