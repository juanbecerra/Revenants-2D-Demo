extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	prepare_coordinate_boxes()
	pass # Replace with function body.

func _on_NewLevelButton_pressed():
	get_tree().change_scene('res://dungeon.tscn')

func _on_MainMenuButton_pressed():
	get_tree().change_scene('res://MainMenu/MainMenu.tscn')

func prepare_coordinate_boxes():
	$CenterContainer/VBoxContainer/XSIZE.add_item("5", 0)
	$CenterContainer/VBoxContainer/XSIZE.add_item("10", 1)
	$CenterContainer/VBoxContainer/XSIZE.add_item("15", 2)
	$CenterContainer/VBoxContainer/XSIZE.add_item("20", 3)
	$CenterContainer/VBoxContainer/XSIZE.add_item("25", 4)
	$CenterContainer/VBoxContainer/XSIZE.add_item("50", 5)
	$CenterContainer/VBoxContainer/XSIZE.add_item("100", 6)
	
	$CenterContainer/VBoxContainer/YSIZE.add_item("5", 0)
	$CenterContainer/VBoxContainer/YSIZE.add_item("10", 1)
	$CenterContainer/VBoxContainer/YSIZE.add_item("15", 2)
	$CenterContainer/VBoxContainer/YSIZE.add_item("20", 3)
	$CenterContainer/VBoxContainer/YSIZE.add_item("25", 4)
	$CenterContainer/VBoxContainer/YSIZE.add_item("50", 5)
	$CenterContainer/VBoxContainer/YSIZE.add_item("100", 6)

func _on_BackButton_pressed():
	get_tree().change_scene('res://MainMenu/MainMenu.tscn')
