extends Sprite

signal level_finished

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	

func _on_Area2D_body_entered(body):
	if "troll" in body.name:
		emit_signal("level_finished")
